package com.profile.resource;

import com.profile.dto.CityDTO;
import com.profile.model.City;
import com.profile.service.CityService;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

@Path("/cities")
public class CityResource {
    private static final Logger LOGGER = Logger.getLogger(CityResource.class);
    private AtomicLong counter = new AtomicLong(0);
    final long invocationNumber = counter.getAndIncrement();
    long started = System.currentTimeMillis();

    @Inject
    CityService cityService;


    @GET()
    @Timeout(250)
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public City findById(@PathParam("id") Long id) {
        try {
            randomDelay();
            LOGGER.infof("CityResource#find() invocation #%d returning successfully", invocationNumber);
            return cityService.findById(id);
        } catch (InterruptedException e) {
            LOGGER.errorf("CityResource#find() invocation #%d timed out after %d ms",
                    invocationNumber, System.currentTimeMillis() - started);
            return null;
        }
    }

    @GET
    @Retry(maxRetries = 4)
    @Produces(MediaType.APPLICATION_JSON)
    public List<City> findAll(@QueryParam(value = "name") String name) {
        return cityService.findAll(name);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void save(CityDTO city) {
        cityService.save(city);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
        cityService.delete(id);
    }

    private void randomDelay() throws InterruptedException {
        Thread.sleep(new Random().nextInt(500));
    }


}
