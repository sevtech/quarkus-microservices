package com.profile.config;

import com.profile.dto.CityDTO;
import com.profile.model.City;
import com.profile.service.CityService;
import io.quarkus.runtime.Startup;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Startup
@ApplicationScoped
public class EagerAppBean {

    @Inject
    CityService cityService;

    EagerAppBean() {
        CityDTO city = CityDTO.builder().cityCode("SVQ").name("Sevilla").build();
    }
}
