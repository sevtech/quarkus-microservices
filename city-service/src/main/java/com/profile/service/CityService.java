package com.profile.service;

import com.profile.dto.CityDTO;
import com.profile.model.City;

import java.util.List;

public interface CityService {
    void save(CityDTO city);
    void delete(Long city);
    List<City> findAll(String name);
    City findById(Long id);



}
