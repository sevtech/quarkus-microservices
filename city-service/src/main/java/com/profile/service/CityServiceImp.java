package com.profile.service;

import com.profile.dto.CityDTO;
import com.profile.model.City;
import com.profile.repository.CityRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@ApplicationScoped
public class CityServiceImp implements CityService {

    @Inject
    CityRepository cityRepository;

    @Override
    @Transactional
    public void save(CityDTO city) {
        cityRepository.persist(City.builder().cityCode(city.getCityCode()).name(city.getName()).build());
    }

    @Override
    @Transactional
    public void delete(Long id) {
        cityRepository.delete("id", id);
    }

    @Override
    public List<City> findAll(String name) {
        if (name != null) {
            return cityRepository.find("name", name).list();
        } else {
            return cityRepository.findAll().list();
        }
    }

    @Override
    public City findById(Long id) {
        return cityRepository.findById(id);
    }

    private void randomDelay() throws InterruptedException {
        Thread.sleep(new Random().nextInt(500));
    }



}
