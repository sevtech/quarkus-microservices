package com.profile.repository;

import com.profile.model.City;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class CityRepository implements PanacheRepositoryBase<City, Long> {

}
