package com.profile.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class City {
    @Id
    @GeneratedValue
    private Long id;
    private String cityCode;
    private String name;


}
