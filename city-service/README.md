# cities-service project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application can be packaged using 
```
./mvnw package
```

It produces the `cities-service-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/cities-service-1.0.0-SNAPSHOT-runner.jar`.

## Creating a dockerfile

To create a docker file, it's necessary to add docker-image extension:

Exist two ways:

 - With mvn command 
 ```
./mvnw quarkus:add-extension -Dextensions="container-image-docker"
``` 

- Add dependency manually to pom.xml:
 ```
        <dependency>
            <groupId>io.quarkus</groupId>
            <artifactId>quarkus-container-image-docker</artifactId>
        </dependency>
``` 

Once you have the extension, execute an mvn command to create image docker

 ```
./mvnw clean package -Dquarkus.container-image.build=true
``` 
Last step is run the image:
 ```
docker run -p 8080:8080 username/user-service:1.0-SNAPSHOT
``` 
