# Quarkus Microservices

> Necessary JDK 11 to compile

## Overview


It consists of two Rest services, *user-service* and *city-service*:
*  User has communication with City
*  City is implemented with fault tolerance

## Previous steps


Compile city-service:

```sh
mvn -f city-service/pom.xml clean install
```

## Up all services and mongodb

```sh
docker-compose up --build
```

The *user-service* is running as native executable, while the *city-service* is running as jar execuutable to compare starting time.




