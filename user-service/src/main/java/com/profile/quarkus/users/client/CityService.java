package com.profile.quarkus.users.client;

import com.profile.quarkus.users.dto.CityDTO;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/cities")
@RegisterRestClient(configKey = "city-api")
public interface CityService {

    @GET()
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    CityDTO findByName(@PathParam String name);

}
