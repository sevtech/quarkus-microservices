package com.profile.quarkus.users.service;

import com.profile.quarkus.users.client.CityService;
import com.profile.quarkus.users.dto.CityDTO;
import com.profile.quarkus.users.dto.UserRequest;
import com.profile.quarkus.users.entity.User;
import com.profile.quarkus.users.repository.UserRepository;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class UserServiceImpl implements UserService {

    @Inject
    UserRepository userRepository;

    @Inject
    @RestClient
    CityService cityService;

    @Override
    public List<User> findAll() {
        return userRepository.findAll().list();
    }

    @Override
    public void create(UserRequest request) {
        CityDTO city = cityService.findByName(request.getCity());
        userRepository.persist(User.builder()
                .age(request.getAge())
                .name(request.getName())
                .cityId(city.getId())
                .build());
    }

    @Override
    public User findByName(String name) {
        return userRepository.find("name", name).firstResult();
    }

    @Override
    public void delete(String name) {
        userRepository.delete("name", name);
    }
}
