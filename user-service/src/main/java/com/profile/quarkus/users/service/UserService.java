package com.profile.quarkus.users.service;

import com.profile.quarkus.users.dto.UserRequest;
import com.profile.quarkus.users.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    void create(UserRequest request);

    User findByName(String name);

    void delete(String name);
}
