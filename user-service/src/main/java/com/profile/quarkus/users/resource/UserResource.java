package com.profile.quarkus.users.resource;

import com.profile.quarkus.users.dto.UserRequest;
import com.profile.quarkus.users.entity.User;
import com.profile.quarkus.users.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/users")
public class UserResource {

    @Inject
    UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findAll() {
        return userService.findAll();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(UserRequest request) {
        userService.create(request);
    }

    @GET()
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User findByName(@PathParam("name") String name) {
        return userService.findByName(name);
    }

    @DELETE
    @Path("/{name}")
    public void delete(@PathParam("name") String name) {
        userService.delete(name);
    }

}