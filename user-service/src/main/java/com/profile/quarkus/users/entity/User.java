package com.profile.quarkus.users.entity;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.codecs.pojo.annotations.BsonProperty;

@MongoEntity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends PanacheMongoEntity {

    private String name;
    private int age;
    @BsonProperty("city-id")
    private int cityId;

}
