package com.profile.quarkus.users;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class UserResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/users")
                .then()
                .statusCode(200)
                .body(is("[{\"age\":0,\"cityId\":0,\"id\":\"5ea32e59b187037a0497cd9c\",\"name\":\"Jesús\"},{\"age\":12,\"cityId\":2,\"id\":\"5ea40ba77d472a3400370e69\",\"name\":\"prueba\"}]"));
    }

}